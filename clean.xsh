#!/usr/bin/env xonsh --no-rc

from buildconfig import *

if rm_extract:
    rm -rf @(extract_a)
if rm_java:
    rm -rf @(javadir_a)
if rm_java_ir:
    rm -rf @(javaird_a)
if rm_apk:
    rm -rf @(outdir_a)
if rm_out_apk:
    rm -rf @(outfile_a)

