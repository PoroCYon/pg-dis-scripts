#!/usr/bin/env xonsh --no-rc

from buildconfig import *

def build_split():
    if do_unsplit:
        print("splitting files...")

        pushd @(assetsdir)
        for f in $(ls *.split0).split('\n'):
            if f != '':
                spln  = f[:-1]
                basen = f[:-len(".split0")]
                print("splitting '" + basen + "'");

                pushd -q @(extract_a)
                if $(ls @(basen)) != '':
                    @(utildir + "/split.fsx") -- @(basen)

                    for g in $(find -name @(spln + "*")).split('\n'):
                        if g != '':
                            mv @(g) @(assetsdir)
                popd -q

def build_apktool():
    print("executing apktool...")

    args = [ "b" ]

    if apk_b_metainf:
        args.append("-c")
    if apk_b_debug:
        args.append("-d")
    if apk_b_force:
        args.append("-f")

    if apktool_verbose:
        args.append("-v")
    else:
        if apktool_quiet:
            args.append("-q")

    args.append("-o")
    args.append(outfile_a)
    args.append(outdir_a)

    exec_cmd(apktool, args)

build_split()
build_apktool()

