
open System
open System.Numerics
open Microsoft.Xna.Framework

(* note: only works on earth *)
(* I'm sorry, extra-terrastrials and astronauts *)

let earthr = 6371.0
let earthi = 1.0 / 6371.0
let rperd  = Math.PI / 180.0
let dperr  = 180.0 / Math.PI

type Geo  = { lat: float, lon: float, h: float (* relative to sea level *) }
type GeoD = Geo * float

let inline toRad d = d * rperd
let inline toDeg r = r * dperr

let geoToCart (p: Geo) =
    let latr = toRad p.lat
    let lonr = toRad p.lon
    let latc = cos latr
    let late = latc * earthr
    in Vector3 (
                    late              * cos (toRad p.lon),
                    late              * sin (toRad p.lon),
           earthr * sin (toRad p.lat) + p.h
       )
let cartToGeo (p: Vector3) =
    let lats = p.Z * earthi
    in {
           asin  lats       |> toDeg,
           atan2 (p.Y, p.X) |> toDeg,
           p.Z - earthr * lats
       }

let trilaterate (ag: V2D) (bg: V2D) (cg: V2D) =
    let ap = geoToCart ag
    let bp = geoToCart bg
    let cg = geoToCart cg

    let ex = let d = bp - ap in d / Vector3.Normalize d
    let i  = Vector2.Dot (ex, cp - ap)
    let ey = let d = cp - ap - i * ex in d / Vector3.Normalize d
    let ez = Vector3.Cross (ex, ey)
    let d  = Vector3.Normalize (bp - ap)
    let j  = Vector3.Dot (ey, cp - ap)

    let x = (ag.d * ag.d - bd.d * bd.d + d * d) / (2.0 * d)
    let y = (ag.d * ag.d - bd.d * bd.d + i * i + j * j) / (2.0 * j) - (i / j) * x
    Vector3 (
        x, y,
        sqrt (ag.d * ag.d - x * x - y * y)
    ) |> cartToGeo
