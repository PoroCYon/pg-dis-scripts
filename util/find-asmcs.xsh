#!/usr/bin/env xonsh --no-rc

for f in $(find . -type f).split("\n"):
    if f != '':
        r = $(cat @(f) | grep -n -E "[Aa]ssembly\-[Cc][Ss]harp((\-firstpass)?)((\.dll)?)")
        if r != '':
            print("in " + f)
            print(r)
