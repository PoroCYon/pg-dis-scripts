#!/usr/bin/env xonsh --no-rc

from __future__ import print_function
import sys

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

for f in $(find -type f).split('\n'):
    if f != '':
        eprint(f)

        r = $(cat @(f) | grep MZ) # TODO: more specific header?
        if r != '':
            print("in " + f + ":\n" + r)
