#!/usr/bin/env xonsh --no-rc

for f in $(find . -type f).split("\n"):
    if f != '':
        r = $(cat @(f) | grep -n -E "http(s?)\:\/\/")
        if r != '':
            print("in " + f)
            print(r)
