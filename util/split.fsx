#!/usr/bin/env fsharpi

open System
open System.IO

let M = 1024 * 1024

let fsiargs = fsi.CommandLineArgs |> Seq.skipWhile ((<>) "--") |> Seq.tail |> Array.ofSeq

let main (args: string array) = do
    let fn = args.[0] // file name

    use fs = File.OpenRead (fn)
    use br = new BinaryReader (fs)

    let mutable n = 0    // current split #
    let mutable r = true

    while r do
        let mutable b: byte array = Array.zeroCreate M
        let c = br.Read (b, 0, M)

        if c <> M then do
            if n <> 0 then do
                Array.Resize (&b, c)

            r <- false

        File.WriteAllBytes (fn + ".split" + string n, b)
        n <- n + 1

    br.Close ()
    fs.Close ()

main fsiargs
