#!/usr/bin/env fsharpi

open System
open System.IO

let fsiargs = fsi.CommandLineArgs |> Seq.skipWhile ((<>) "--") |> Seq.tail |> Array.ofSeq

let collectSplits f n = [ 0 .. n ] |> List.map (fun i -> f + ".split" + string i)

let main (args: string array) = do
    let fn = args.[0]        // target file name
    let n  = args.[1] |> int // last split #

    use fs = File.OpenWrite (fn) // dest file stream
    use bw = new BinaryWriter (fs)

    for f in collectSplits fn n do
        let data = File.ReadAllBytes (f)
        in bw.Write (data)

    bw.Close ()
    fs.Close ()

main fsiargs
