from copy import deepcopy

def log_spawn(pokes, pokelist):
    with open('spawns.json', 'r+') as f:
        write = False
        content = f.read()
        j = json.loads(content)
        jj = deepcopy(j)
        for poke in pokes:
            exists = False
            for index, spawn in enumerate(j['spawns']):
                if spawn['spawnid'] == poke.SpawnPointId:
                    exists = True
                    if not any(int(p['id']) == poke.pokemon.PokemonId for p in spawn['pokemon']):
                        newpoke = deepcopy(j['spawns'][0]['pokemon'][0])
                        newpoke['id'] = poke.pokemon.PokemonId
                        newpoke['name'] = pokelist[poke.pokemon.PokemonId - 1]['Name']
                        jj['spawns'][index]['pokemon'].append(newpoke)
                        write = True

            if exists == False: 
                newspawn = deepcopy(j['spawns'][0])
                newspawn['spawnid'] = poke.SpawnPointId
                newspawn['coords']['lat'] = poke.Latitude
                newspawn['coords']['lng'] = poke.Longitude
                newspawn['pokemon'][0]['id'] = poke.pokemon.PokemonId
                newspawn['pokemon'][0]['name'] = pokelist[poke.pokemon.PokemonId - 1]['Name']
                jj['spawns'].append(newspawn)
                write = True

        if write:
            f.seek(0)
            json.dump(jj, f)
