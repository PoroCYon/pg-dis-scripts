#!/usr/bin/env xonsh --no-rc

for f in $(find -type f).split('\n'):
    if f != '':
        r = $(cat @(f) | grep -bE -e "PNG" -e "IHDR") # should be good enough
        if r != '':
            print('in ' + f)
            print(r)
