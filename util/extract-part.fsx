#!/usr/bin/env fsharpi

open System
open System.IO

let fsiargs = fsi.CommandLineArgs |> Seq.skipWhile ((<>) "--") |> Seq.tail |> Array.ofSeq

let main (args: string array) = do
    let fn = args.[0]
    let st = args.[1] |> int
    let ln = args.[2] |> int

    use fs = File.OpenRead    (fn)
    use br = new BinaryReader (fs)

    let mutable b: byte array = Array.zeroCreate (max st ln)

    let l = br.Read (b, 0, st) // skip first <st> bytes
    if l <> st then do printfn "Warning: did not skip %i bytes." st

    let l' = br.Read (b, 0, ln)
    if l' <> ln then do printfn "Warning: did not read %i bytes." ln

    if Array.length b <> ln then do Array.Resize (&b, ln)

    File.WriteAllBytes(fn + "-part", b)

main fsiargs
