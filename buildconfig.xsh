#!/usr/bin/env xonsh --no-rc

import os
basedir = os.getcwd()

# tools used
tar      = "bsdtar"
apktool  = "apktool"
apkunzip = "7z"
dex2jar  = "dex2jar"
procyon  = "procyon-decompiler"
jadx     = "jadx"
zipcmd   = "xz"

# string params (files etc)
infile  = "pkmn-go.apk"     # used by apktool d
outdir  = "apk"             # ^
outfile = "pkmn-go-out.apk" # used by apktool b
extract = "extract"
javadir = "java"
javaird = "java-ir"
audiotb = "pkmn-go-audio"
# absolute versions
infile_a  = basedir + "/" + infile
outdir_a  = basedir + "/" + outdir
outfile_a = basedir + "/" + outfile
extract_a = basedir + "/" + extract
javadir_a = basedir + "/" + javadir
javaird_a = basedir + "/" + javaird
# more paths, for good measure
assetsdir = outdir_a  + "/assets/bin/Data"
audiodir  = extract_a + "/audio"
utildir   = basedir   + "/util"
uassetdir = extract_a + "/assets"

# bool params
apktool_verbose = False # -v
apktool_quiet   = False # -q
apktool_cleanso = True  # remove fake .so files & backups placed
                        # by the makefile in ./so-hack

apk_d_nodebug = False # d -b
apk_d_forced  = False # d -f

apk_b_metainf = False # b -c
apk_b_debug   = True  # b -d
apk_b_force   = False # b -f

do_dex2jar    = True # and do_extract
dex2jar_debug = True # -d
dex2jar_overw = True # -f

do_procyon    = True # and do_dex2jar
procyon_lines = True # -ln
procyon_wcimp = True # -ci

do_jadx  = True  # and do_dex2jar
jadx_sbc = False
jadx_deo = False

do_extract  = True
do_unsplit  = True
do_exaudio  = True  # and do_unsplit
do_zipaudio = True  # and do_exaudio

do_disunity = True  # and do_unsplit
disu_json   = False # for json output, whenever possible
disu_list   = False # write object lists

do_split = True

# params for clean.xsh
rm_extract = True
rm_java    = True
rm_java_ir = True
rm_apk     = True
rm_out_apk = True

# stuff sometimes doesn't want to work
def exec_cmd(cmd, args):
    aliases["cmdcmdcmd" + cmd] = [ cmd ] + list(filter(lambda s: s != '', args))
    $[@("cmdcmdcmd" + cmd)]
    aliases["cmdcmdcmd" + cmd] = None
def exec_cmd_va(cmd, *args):
    exec_cmd(cmd, args)
