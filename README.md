# Pokémon Go disassembly scripts

To much surprise, it does as described on the tin. Certified to Work on My Machine.

---

## Usage

Place your Pokémon Go APK in the main directory, with the name `pkmn-go.apk`.
Then, run `./disasm.xsh` (You can tweak the settings in `buildconfig.xsh` first,
if you want to). To rebuild, run `./build.xsh`.

### Requirements

* `xonsh`, to execute the `.xsh` files.
* `apktool`, to extract everything from your .apk file.
* `fsharp`, to split/unsplit files.
* `dex2jar` (optional): .dex to .jar conversion.
* `jadx` (optional): java decompilation.
* `procyon` (optional): java disassembly (this decompiler chokes on a certain file), requires dex2jar.

## `so-hacks`

This directory contains C code to proxy JNI and Unity calls to `libunity.so` and `libNianticLabsPlugin.so`.
It can be used to execute arbitrary native code, intercept stuff, etc.

### Requirements

* `jdk`: Either 7 or 8, required for JNI calls.
* A GCC cross-compiler to 32-bit ARM, to compile the C code.
* A GAS cross-compiler to 32-bit ARM, to compile the ARM assembly code.
* The standard C library headers for the cross-compiler.

