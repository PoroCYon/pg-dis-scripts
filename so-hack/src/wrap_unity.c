
#include "libunity.h"
#include "wrap_unity.h"

void CallUnitySendMessage(
    const char* ob, const char* method, const char* msg)
{
    UnitySendMessage(ob, method, msg);
}

JNIEXPORT jint JNICALL CallJNI_OnLoad(
    JavaVM* vm, void* reserved)
{
    return JNI_OnLoad(vm, reserved);
}
JNIEXPORT void JNICALL CallJNI_OnUnload(
    JavaVM* vm, void* reserved)
{
    JNI_OnUnload(vm, reserved);
}

