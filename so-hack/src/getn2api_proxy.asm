
CODE:
    .globl GetN2Api
    .extern CallGetN2Api
    .text

GetN2Api:
    push { r4, lr }
    bl CallGetN2Api
    pop { r4, pc }

