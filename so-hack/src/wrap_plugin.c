
#include "niantic_labs_plugin.h"
#include "wrap_plugin.h"

void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API CallUnityPluginLoad(IUnityInterface* ifaces)
{
    UnityPluginLoad(ifaces); // in libNianticPlugin.so
}
void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API CallUnityPluginUnload()
{
    UnityPluginUnload();
}

JNIEXPORT void JNICALL CallJava_com_nianticlabs_nia_contextservice_ContextService_setActivityProviderClass(
    JNIEnv* env, jobject obj,
    jstring str)
{
    Java_com_nianticlabs_nia_contextservice_ContextService_setActivityProviderClass(env, obj, str);
}
JNIEXPORT void JNICALL CallJava_com_nianticlabs_nia_javawrap_NianticPluginWrapper_nativeInitialize(
    JNIEnv* env, jobject obj)
{
    Java_com_nianticlabs_nia_javawrap_NianticPluginWrapper_nativeInitialize(env, obj);
}
JNIEXPORT jlong JNICALL CallJava_com_nianticlabs_nia_javawrap_NianticPluginWrapper_nativeGetApi(
    JNIEnv* env, jobject obj)
{
    return Java_com_nianticlabs_nia_javawrap_NianticPluginWrapper_nativeGetApi(env, obj);
}
JNIEXPORT void JNICALL CallJava_com_niantaiclabs_nia_javawrap_NianticPluginWrapper_nativeDispose(
    JNIEnv* env, jobject obj)
{
    Java_com_nianticlabs_nia_javawrap_NianticPluginWrapper_dispose(env, obj);
}
JNIEXPORT void JNICALL CallJava_com_nia_unity_UnityUtil_nativeInit(
    JNIEnv* env, jobject obj)
{
    Java_com_nianticlabs_nia_unity_UnityUtil_nativeInit(env, obj);
}

JNIEXPORT jint JNICALL CallJNI_OnLoad(
    JavaVM* vm, void* reserved)
{
    return JNI_OnLoad(vm, reserved);
}

