
CODE:
    .globl CallGetN2Api
    .extern GetN2Api
    .text

CallGetN2Api:
    push { r4, lr }
    bl GetN2Api
    pop { r4, pc }

