
#define PROXY_LIB

#include "niantic_labs_plugin.h"
#include "wrap_plugin.h"

void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API UnityPluginLoad(IUnityInterface* ifaces)
{
    // TODO: ping

    CallUnityPluginLoad(ifaces);
}
void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API UnityPluginUnload()
{
    CallUnityPluginUnload();

    // TODO: ping
}

JNIEXPORT void JNICALL Java_com_nianticlabs_nia_contextservice_ContextService_setActivityProviderClass(
    JNIEnv* env, jobject obj, jstring str)
{
    CallJava_com_nianticlabs_nia_contextservice_ContextService_setActivityProviderClass(env, obj, str);
}
JNIEXPORT void JNICALL Java_com_nianticlabs_nia_javawrap_NianticPluginWrapper_nativeInitialize(
    JNIEnv* env, jobject obj)
{
    CallJava_com_nianticlabs_nia_javawrap_NianticPluginWrapper_nativeInitialize(env, obj);
}
JNIEXPORT jlong JNICALL Java_com_nianticlabs_nia_javawrap_NianticPluginWrapper_nativeGetApi(
    JNIEnv* env, jobject obj)
{
    return CallJava_com_nianticlabs_nia_javawrap_NianticPluginWrapper_nativeGetApi(env, obj);
}
JNIEXPORT void JNICALL Java_com_nianticlabs_nia_javawrap_NianticPluginWrapper_nativeDispose(
    JNIEnv* env, jobject obj)
{
    CallJava_com_nianticlabs_nia_javawrap_NianticPluginWrapper_nativeDispose(env, obj);
}
JNIEXPORT void JNICALL Java_com_nianticlabs_nia_unity_UnityUtil_nativeInit(
    JNIEnv* env, jobject obj)
{
    CallJava_com_nianticlabs_nia_unity_UnityUtil_nativeInit(env, obj);
}

JNIEXPORT jint JNICALL JNI_OnLoad(
    JavaVM* vm, void* reserved)
{
    return CallJNI_OnLoad(vm, reserved);
}

// don't know the sig -> embed it here
/*char GetN2Api[0x20] = {
    0x10, 0xB5, 0x04, 0x46, // ?                  // invalid ARM?
    0xFF, 0xF7, 0x26, 0xFB, // bl 0x4850C         // bogus?
    0x04, 0x49,             // ldr r1, [pc, 0x10]
    0x20, 0x46,             // mov r0, r4
    0xBC, 0x22,             // movs r2, 0xBC
    0x79, 0X44,             // add r1, pc
    0x04, 0x31,             // adds r1, 4
    0xBD, 0xE8, 0x10, 0x40, // pop.w {r4, lr}     // bogus?
    0x7C, 0xF1, 0x03, 0xB8, // b.w 0x1C4ED8       // bogus?
    0x00, 0xBF,             // nop
    0xF6, 0xC7,             // stm r7!, {r1, r2, r4, r5, r6, r7} // bogus?
    0x1A, 0x00              // movs r2, r3
};*/

