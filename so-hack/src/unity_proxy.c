
#define PROXY_LIB

#include "libunity.h"
#include "wrap_unity.h"

void UnitySendMessage(const char* ob, const char* method, const char* msg)
{
    CallUnitySendMessage(ob, method, msg);
}

JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM* vm, void* reserved)
{
    return CallJNI_OnLoad(vm, reserved);
}
JNIEXPORT void JNICALL JNI_OnUnload(JavaVM* vm, void* reserved)
{
    CallJNI_OnUnload(vm, reserved);
}

