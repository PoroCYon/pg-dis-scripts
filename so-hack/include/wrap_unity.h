
#ifndef WRAP_UNITY_H_
#define WRAP_UNITY_H_

#include <jni.h>

//! IF YOU UPDATE THIS, DON'T FORGET TO EDIT libunity.h

#ifdef __cplusplus
extern "C"
{
#endif
    void CallUnitySendMessage(const char* ob, const char* method, const char* msg);

    JNIEXPORT jint JNICALL CallJNI_OnLoad(
        JavaVM* vm, void* reserved
    );
    JNIEXPORT void JNICALL CallJNI_OnUnload(
        JavaVM* vm, void* reserved
    );
#ifdef __cplusplus
}
#endif

#endif

