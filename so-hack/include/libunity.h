
#ifndef LIBUNITY_H_
#define LIBUNITY_H_

#include <jni.h>

#ifndef PROXY_LIB
#define LUP_EXTERN extern
#else
#define LUP_EXTERN
#endif

//! IF YOU UPDATE THIS, DON'T FORGET TO EDIT wrap_unity.h

#ifdef __cplusplus
extern "C"
{
#endif
    LUP_EXTERN void UnitySendMessage(const char* obj, const char* method, const char* msg);

    LUP_EXTERN JNIEXPORT jint JNICALL
        JNI_OnLoad(
            JavaVM* vm, void* reserved
        );
    LUP_EXTERN JNIEXPORT void JNICALL
        JNI_OnUnload(
            JavaVM* vm, void* reserved
        );
#ifdef __cplusplus
}
#endif

#endif

