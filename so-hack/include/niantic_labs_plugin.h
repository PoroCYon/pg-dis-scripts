
#ifndef NIANTIC_LABS_PLUGIN_H_
#define NIANTIC_LABS_PLUGIN_H_

#include "Unity/IUnityInterface.h"
#include <jni.h>

#ifndef PROXY_LIB
#define NLP_EXTERN extern
#else
#define NLP_EXTERN
#endif

//! IF YOU UPDATE THIS, DON'T FORGET TO EDIT wrap_plugin.h

#ifdef __cplusplus
extern "C"
{
#endif
    // Unity
    NLP_EXTERN void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API
        UnityPluginLoad(
            IUnityInterface* ifaces
        );
    NLP_EXTERN void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API
        UnityPluginUnload(

        );

    // JNI stuff
    NLP_EXTERN JNIEXPORT void JNICALL
        Java_com_nianticlabs_nia_contextservice_ContextService_setActivityProviderClass(
            JNIEnv* env, jobject obj,
            jstring str
        );
    NLP_EXTERN JNIEXPORT void JNICALL
        Java_com_nianticlabs_nia_javawrap_NianticPluginWrapper_nativeInitialize(
            JNIEnv* env, jobject obj
        );
    NLP_EXTERN JNIEXPORT jlong JNICALL
        Java_com_nianticlabs_nia_javawrap_NianticPluginWrapper_nativeGetApi(
            JNIEnv* env, jobject obj
        );
    NLP_EXTERN JNIEXPORT void JNICALL
#ifdef NLP_EXTERN
        Java_com_nianticlabs_nia_javawrap_NianticPluginWrapper_dispose
        /* ^ a bug! (it's exported like this in the original lib) */
#else
        Java_com_nianticlabs_nia_javawrap_NianticPluginWrapper_nativeDispose
#endif
        (
            JNIEnv* env, jobject obj
        );
    NLP_EXTERN JNIEXPORT void JNICALL
        Java_com_nianticlabs_nia_unity_UnityUtil_nativeInit(
            JNIEnv* env, jobject obj
        );

    NLP_EXTERN JNIEXPORT jint JNICALL
        JNI_OnLoad(
            JavaVM* vm, void* reserved
        );

    // TODO: do this in a non-hacky way
    NLP_EXTERN char GetN2Api[0x20];
#ifdef __cplusplus
}
#endif

#endif

