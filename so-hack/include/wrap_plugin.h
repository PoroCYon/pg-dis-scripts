
#ifndef WRAP_PLUGIN_H_
#define WRAP_PLUGIN_H_

#include "Unity/IUnityInterface.h"
#include <jni.h>

//! IF YOU UPDATE THIS, DON'T FORGET TO EDIT niantic_labs_plugin.h

#ifdef __cplusplus
extern "C"
{
#endif
    void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API
        CallUnityPluginLoad(
            IUnityInterface* ifaces
        );
    void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API
        CallUnityPluginUnload(

        );

    JNIEXPORT void JNICALL
        CallJava_com_nianticlabs_nia_contextservice_ContextService_setActivityProviderClass(
            JNIEnv* env, jobject obj,
            jstring str
        );
    JNIEXPORT void JNICALL
        CallJava_com_nianticlabs_nia_javawrap_NianticPluginWrapper_nativeInitialize(
            JNIEnv* env, jobject obj
        );
    JNIEXPORT jlong JNICALL
        CallJava_com_nianticlabs_nia_javawrap_NianticPluginWrapper_nativeGetApi(
            JNIEnv* env, jobject obj
        );
    JNIEXPORT void JNICALL
        CallJava_com_nianticlabs_nia_javawrap_NianticPluginWrapper_nativeDispose(
            JNIEnv* env, jobject obj
        );
    JNIEXPORT void JNICALL
        CallJava_com_nianticlabs_nia_unity_UnityUtil_nativeInit(
            JNIEnv* env, jobject obj
        );

    JNIEXPORT jint JNICALL
        CallJNI_OnLoad(
            JavaVM* vm, void* reserved
        );
#ifdef __cplusplus
}
#endif

#endif

