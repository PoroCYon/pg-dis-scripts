# KOPIMI LICENSE

> Version 1, July 2016
>
> Written by PoroCYon, this license text is
> licensed under the Kopimi license version 1.

## Kopimi license: Terms and Conditions

> Copyright (c) 2016 PoroCYon

* Any person obtaining a copy of the work is allowed to
  use, study, modify and distribute the work and all modified
  versions of the work, in any way, without any restrictions.
  This license grants to the owner of the copy all patents
  and trademarks concerning the work,
  held by the creator(s) or their representative(s).
  One cannot be charged for requesting a copy, except for the
  material cost of the creation of the copy (e.g. ink and paper).
* The work is provided "as is", it comes with no warranty
  of any kind, to the extent permitted by applicable law
  (including, but not limited to, merchantability and
  fitness for a particular purpose).

***The following part has no legal value at all:***
## TL;DR

* You just DO WHAT THE FUCK YOU WANT TO.
* It's NOT THE CREATOR'S FAULT IF IT DOESN'T FUCKING WORK.

