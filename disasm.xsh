#!/usr/bin/env xonsh --no-rc

import os
import os.path

from buildconfig import *

def disasm_apktool():
    print("executing apktool...")

    args = [ "d" ]

    if apk_d_nodebug:
        args.append("-b")
    if apk_d_forced:
        args.append("-f")
    if apktool_verbose:
        args.append("-v")
    else:
        if apktool_quiet:
            args.append("-q")

    args.append("-o")
    args.append(outdir_a)
    args.append(infile_a)

    exec_cmd(apktool, args)

    if apktool_cleanso:
        print("cleaning .so backups")
        # the rest is overwritten by apktool,
        # this makes the makefile create new backups
        # instead of the old ones (of a potentially older version)
        $[rm @(outdir_a + "/lib/armeabi-v7a/*.bak.so")]

def disasm_procyon():
    if do_procyon:
        print("disassembling using procyon...")

        $[mkdir @(javaird_a)]

        args = [ "-o", javaird_a, "-r", "-eml" ]

        if procyon_lines:
            args.append("-ln")
        if procyon_wcimp:
            args.append("-ci")

        args.append(extract_a + "/classes.jar")

        exec_cmd(procyon, args)
def disasm_jadx():
    if do_jadx: # procyon hangs on com/google/android/gms/internal/zzsi -> have to use jadx
        print("decompiling using jadx...")

        $[mkdir @(javadir_a)]

        args = [ "-d", javadir_a ]

        if jadx_sbc:
            args.append("-sbc")
        if jadx_deo:
            args.append("-deo")

        args.append(extract_a + "/classes.dex")

        exec_cmd(jadx, args)
def disasm_dex2jar():
    if do_dex2jar:
        print("converting dex to jar...")

        args = []

        if dex2jar_debug:
            args.append("-d")
        if dex2jar_overw:
            args.append("-f")

        args.append("-o")
        args.append(extract_a + "/classes.jar")
        args.append(extract_a + "/classes.dex")

        exec_cmd(dex2jar, args)

        disasm_procyon()
        disasm_jadx()

def disasm_extract():
    if do_extract:
        print("extracting classes.dex and resources.arsc...")

        $[mkdir @(extract_a)]
        $[@(apkunzip) e @(infile_a) -y "classes.dex" "resources.arsc" @("-o" + extract_a)]

        disasm_dex2jar()

def disasm_zipaudio():
    if do_zipaudio:
        print("zipping audio...")

        $[pushd -q @(audiodir)]
        tarball = extract_a + "/" + audiotb + ".tar"
        $[@(tar) -cf @(tarball) *.ogg]
        $[@(zipcmd) -c @(tarball) > @(tarball + "." + zipcmd)] # mit liebe verpackt, ganz für sie
        $[rm @(tarball)]
        $[popd -q]

def disasm_exaudio():
    if do_exaudio:
        print("extracting audio...")

        $[mkdir @(audiodir)]

        for d in [ assetsdir, extract_a ]:
            $[pushd -q @(d)]
            $[@(utildir + "/python-fsb5/extract.py") -o @(audiodir) *.resource]
            $[popd -q]

        disasm_zipaudio()

def disasm_disu_file(f):
    print("unpacking " + f)
    disu = utildir + "/disunity/disunity.sh"
    fdir, base = os.path.split(f)
    fnwe, ext  = os.path.splitext(base)

    $[@(disu) asset unpack -l 2 @(f)]

    destdir = ""
    if base == fnwe: # no extension
        destdir = f + '_'
    else:
        destdir = fnwe

    newddir = uassetdir + '/' + base

    $[mv @(destdir) @(newddir)]

    ofmat = "TEXT"
    ofext = ".txt"
    if disu_json:
        ofmat = "JSON"
        ofext = ".json"

    outfbase = newddir + '/' + fnwe

    if disu_list:
        # can't do these in 1 invocation .__.
        $[@(disu) asset blocks    -f @(ofmat) @(f) > @(outfbase + ".blocks"    + ofext)]
        $[@(disu) asset externals -f @(ofmat) @(f) > @(outfbase + ".externals" + ofext)]
        $[@(disu) asset header    -f @(ofmat) @(f) > @(outfbase + ".header"    + ofext)]
        $[@(disu) asset objectids -f @(ofmat) @(f) > @(outfbase + ".objectids" + ofext)]
        $[@(disu) asset objects   -f @(ofmat) @(f) > @(outfbase + ".objects"   + ofext)]
        $[@(disu) asset types     -f @(ofmat) @(f) > @(outfbase + ".types"     + ofext)]
def disasm_disunity():
    if do_disunity:
        print("unpacking unity asset files, this may take a while...")

        $[mkdir @(uassetdir)]

        regexes = [
                "\.assets$"     , # .asset files
                "^[0-9a-f]{32}$", # hex stuff files ('regular' assets)
                "^level[0-9]$"  , # level<n> files

                # 'hardcoded' files
                "^globalgamemanagers$",
                "^unity\ default\ resources$",
                "^unity_builtin_extra$"
            ]
        regex = '(' + '|'.join(regexes) + ')'
        for d in [ assetsdir, assetsdir + "/Resources", extract_a ]:
            $[pushd -q @(d)]
            for f in $(ls ).split('\n'):
                if f != '':
                    # it is an .assets file OR it's a file with a hex name OR it's globalgamemanagers
                    if $(echo @(f) | grep -E @(regex)) != '':
                        disasm_disu_file(f)
            $[popd -q]

def disasm_unsplit():
    if do_unsplit:
        print("gluing together .split files...")

        $[mkdir @(extract_a)]

        $[pushd -q @(assetsdir)]
        for f in $(ls *.split0).split('\n'):
            if f != '':
                spln  = f[:-1]
                basen = f[:-len(".split0")]

                i = 1
                r = True

                while r:
                    if $(ls @(spln + str(i))) != '':
                        i = i + 1
                    else:
                        r = False
                i = i - 1

                print("gluing '" + basen + "'");
                $[@(utildir + "/unsplit.fsx") -- @(basen) @(str(i))]
                $[mv @(basen) @(extract_a)]
        $[popd -q]

        disasm_exaudio ()
        disasm_disunity ()

disasm_apktool()
disasm_extract()
disasm_unsplit()

